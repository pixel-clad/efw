PREFIX = /usr/local

ETC 		= $(DESTDIR)/etc/efw
COMPLETION 	= $(DESTDIR)/etc/bash_completion.d
SBIN 		= $(DESTDIR)/$(PREFIX)/sbin
SYSTEMD 	= $(DESTDIR)/etc/systemd/system

etc/efw/efw6.rules: etc/efw/efw.rules
	sed -E '/ -d (224|239)\./d; s/ -p icmp/ -p ipv6-icmp/; s/--icmp-type/--icmpv6-type/' $< > $@

.PHONY: all
all: etc/efw/efw6.rules

.PHONY: install
install:
	mkdir -p "$(SBIN)"
	cp efw "$(SBIN)"

	mkdir -p "$(ETC)"
	cp etc/efw/efw.rules etc/efw/efw6.rules  "$(ETC)"
	cp --no-clobber etc/efw/local.rules etc/efw/local6.rules "$(ETC)"
	cp --no-clobber etc/efw/efwrc "$(ETC)"
	# install unit file only if systemd seems to be installed
	-test -d "$(SYSTEMD)" && cp etc/systemd/system/efw.service "$(SYSTEMD)"

	mkdir -p "$(COMPLETION)"
	cp etc/bash_completion.d/efw "$(COMPLETION)"

.PHONY: uninstall
uninstall:
	-rm -f "$(SBIN)/efw"
	-rm -f "$(COMPLETION)/efw"
